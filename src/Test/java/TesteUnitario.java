import Bicicleta.*;
import Totem.*;
import Tranca.*;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import io.javalin.plugin.json.JavalinJson;
import io.javalin.plugin.openapi.OpenApiPlugin;
import kong.unirest.Unirest;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

import java.net.http.HttpResponse;
import java.util.Collection;

import org.junit.Test;

import Main.RespostaErro;

public class TesteUnitario {
    

//Testes referentes a Bicicleta
    
public void testeAdicionarBicicleta() {
    Context ctx = mock(Context.class);
    RequisicaoNovaBicicleta bicicletaNova = new RequisicaoNovaBicicleta(11, "Caloi", "Elétrica", "2018", "6", STATUS_ATIVA);
    when(ctx.bodyAsClass(RequisicaoNovaBicicleta.class)).thenReturn(bicicletaNova);
    BicicletaController.adicionarBicicleta(ctx);
    verify(ctx).status(200);
}		

public void testeRemoverBicicleta() {
    Context ctx = mock(Context.class);
    
    when(ctx.queryParam("id")).thenReturn("5"); //return 5 é condicao de sucesso do recebimento do id
    BicicletaController.removerBicicleta(ctx);
    verify(ctx).status(200);
}





//Testes referentes a Totem

public void testeAdicionarTotem() {
    Context ctx = mock(Context.class);
    RequisicaoNovoTotem totemNovo = new RequisicaoNovoTotem(5,"Barra");
    when(ctx.bodyAsClass(RequisicaoNovoTotem.class)).thenReturn(totemNovo);
    TotemController.adicionarTotem(ctx);
    verify(ctx).status(200);
}		

public void testeRemoverTotem() {
    Context ctx = mock(Context.class);
    
    when(ctx.queryParam("id")).thenReturn("5"); //return 5 condicao de sucesso do recebimento do id
    TotemController.removerTotem(ctx);
    verify(ctx).status(200);
}





//Testes referentes a Tranca

public void testeAdicionarTranca() {
    Context ctx = mock(Context.class);
    RequisicaoNovaTranca trancaNova = new RequisicaoNovaTranca(5, 1234, 9, "Barra", "Eletrica", STATUS_INATIVA);
    when(ctx.bodyAsClass(RequisicaoNovaTranca.class)).thenReturn(trancaNova);
    TrancaController.adicionarTranca(ctx);
    verify(ctx).status(200);
}		

public void testeRemoverTranca() {
    Context ctx = mock(Context.class);
    
    when(ctx.queryParam("id")).thenReturn("5"); //return 5 -> sucesso do recebimento do id
    TrancaController.removerTranca(ctx);
    verify(ctx).status(200);
}

public void testeAcharTrancaPorId() {
    Context ctx = mock(Context.class);
		  
    when(ctx.queryParam("id")).thenReturn(null);
    when(ctx.queryParam("numero")).thenReturn("200"); 
    
    TrancaController.acharTrancaPorId(ctx);
    verify(ctx).status(200);
}
	


}




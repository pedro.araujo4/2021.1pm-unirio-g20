package Bicicleta;

public class Bicicleta {
	public int id;
    public String marca;
    public String modelo;
    public String ano;
    public double numero;
	public String status;
	
	public Bicicleta(int id, String marca, String modelo, String ano, double numero, String status) {
		this.id = id;
        this.marca = marca;
        this.modelo = modelo;
        this.ano = ano;
        this.numero = numero;
        this.status = status;
	}
}

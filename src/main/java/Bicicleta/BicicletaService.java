package Bicicleta;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class BicicletaService {
  
    private BicicletaService() {
        throw new IllegalStateException("Classe de BicicletaService");
      }
  
      
    static final String STATUS_ATIVA = "ATIVA";
    static final String STATUS_INATIVA = "INATIVA";

    private static Map<Integer, Bicicleta> redeDeBicicletas = new HashMap<>();
    private static AtomicInteger ultimoId;

    static {
        redeDeBicicletas.put(0, new Bicicleta(0, "Cannondale", "Dobravel", "2015", 1, STATUS_ATIVA));
        redeDeBicicletas.put(1, new Bicicleta(1, "Cervélo", "Urbana", "2014", 2, STATUS_INATIVA));
        redeDeBicicletas.put(2, new Bicicleta(2, "Caloi", "Hibrida", "2013", 3, STATUS_ATIVA));
        redeDeBicicletas.put(3, new Bicicleta(3, "TemBici", "Reclinada", "2019", 4, STATUS_ATIVA));
        redeDeBicicletas.put(4, new Bicicleta(4, "TemBici", "Urbana", "2019", 5, STATUS_ATIVA));
        redeDeBicicletas.put(5, new Bicicleta(5, "Sense", "Dobravel", "2016", 6, STATUS_INATIVA));
        ultimoId = new AtomicInteger(redeDeBicicletas.size());
    }

    // Adiciona uma bicicleta a rede de bicicletas

    public static void adicionarBicicleta(int id, String marca, String modelo, String ano, double numero, String status) {
        int idAdc = ultimoId.incrementAndGet();
        redeDeBicicletas.put(idAdc, new Bicicleta(id, marca, modelo, ano, numero, status));
    }

    // Retorna uma coleção de bicicletas

    public static Collection<Bicicleta> retornaColecao() {
        return redeDeBicicletas.values();
    }

    // Edita uma bicicleta

    public static void atualizarBicicleta(int id, String marca, String modelo, String ano, double numero, String status) {
        redeDeBicicletas.put(id, new Bicicleta(id, marca, modelo, ano, numero, status));
    }

    // Edita um status de uma bicicleta

    public static void atualizarStatusBicicleta(int id, String marca, String modelo, String ano, double numero, String status) {
        redeDeBicicletas.put(id, new Bicicleta(id, marca, modelo, ano, numero, status));
    }
    
    // Acha uma bicicleta por ID

    public static Bicicleta acharBicicletaPorId(int id) {
        return redeDeBicicletas.get(id);
    }

    // Remove uma bicicleta da rede de bicicleta

    public static void removerBicicleta(int id) {
        redeDeBicicletas.remove(id);
    }

}

package Bicicleta;

import Main.RespostaErro;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import io.javalin.plugin.openapi.annotations.*;


public class BicicletaController {

    private BicicletaController() {
        throw new IllegalStateException("Classe de Bicicletaontroller");
      }

    static final String NOT_FOUND_RESPONSE = "Bicicleta não foi encontrada";

        // Cadastrar bicicleta
    
        @OpenApi(
            summary = "Cadastrar bicicleta",
            operationId = "cadastrarBicicleta",
            path = "/Bicicleta",
            method = HttpMethod.POST,
            tags = {"Bicicleta"},
            requestBody = @OpenApiRequestBody(content = {@OpenApiContent(from = RequisicaoNovaBicicleta.class)}),
            responses = {
                    @OpenApiResponse(status = "200"),
                    @OpenApiResponse(status = "422", content = {@OpenApiContent(from = RespostaErro.class)})
            }
    )

        public static void cadastrarBicicleta (Context ctx) {    
            RequisicaoNovaBicicleta bicicleta = ctx.bodyAsClass(RequisicaoNovaBicicleta.class);
            BicicletaService.adicionarBicicleta(bicicleta.id, bicicleta.marca, bicicleta.modelo, bicicleta.ano, bicicleta.numero, bicicleta.status);
            ctx.status(200);
        }
        


        // Editar bicicleta
        // Colocar uma bicicleta nova ou retornando de reparo de volta na rede de totens  
        // Retirar bicicleta para reparo ou aposentadoria
    
        @OpenApi(
            summary = "Editar bicicleta",
            operationId = "editarBicicleta",
            path = "/Bicicleta/:id",
            method = HttpMethod.PATCH,
            pathParams = {@OpenApiParam(name = "id", type = Integer.class, description = "ID de bicicleta")},
            tags = {"Bicicleta"},
            requestBody = @OpenApiRequestBody(content = {@OpenApiContent(from = RequisicaoNovaBicicleta.class)}),
            responses = {
                    @OpenApiResponse(status = "200"),
                    @OpenApiResponse(status = "404", content = {@OpenApiContent(from = RespostaErro.class)}),
                    @OpenApiResponse(status = "422", content = {@OpenApiContent(from = RespostaErro.class)})
            }
    )

        public static void editarBicicleta (Context ctx) {
            Bicicleta bicicleta = BicicletaService.acharBicicletaPorId(caminhoValidoParametroId(ctx));
            if (bicicleta == null) {
                throw new NotFoundResponse(NOT_FOUND_RESPONSE);
            } else {
                RequisicaoNovaBicicleta novaBicicleta = ctx.bodyAsClass(RequisicaoNovaBicicleta.class);
                BicicletaService.atualizarBicicleta(bicicleta.id, novaBicicleta.marca, novaBicicleta.modelo, novaBicicleta.ano, novaBicicleta.numero, novaBicicleta.status);
                ctx.status(200);
            }

        }
    
        // Remover bicicleta

        @OpenApi(
            summary = "Remove bicicleta por ID",
            operationId = "removerBicicleta",
            path = "/Bicicleta/:id",
            method = HttpMethod.DELETE,
            pathParams = {@OpenApiParam(name = "id", type = Integer.class, description = "ID da bicicleta")},
            tags = {"Bicicleta"},
            responses = {
                    @OpenApiResponse(status = "200"),
                    @OpenApiResponse(status = "404", content = {@OpenApiContent(from = RespostaErro.class)})
            }
    )
    
        public static void removerBicicleta (Context ctx) {
        
            Bicicleta bicicleta = BicicletaService.acharBicicletaPorId(caminhoValidoParametroId(ctx));
            if (bicicleta == null) {
                throw new NotFoundResponse(NOT_FOUND_RESPONSE);
            } else {
                BicicletaService.removerBicicleta(bicicleta.id);
                ctx.status(200);
            }
        }

        // Obter bicicleta
    
        @OpenApi(
            summary = "Obter bicicleta por ID",
            operationId = "obterBicicleta",
            path = "/Bicicleta/:id",
            method = HttpMethod.GET,
            pathParams = {@OpenApiParam(name = "id", type = Integer.class, description = "ID da bicicleta")},
            tags = {"Bicicleta"},
            responses = {
                    @OpenApiResponse(status = "200", content = {@OpenApiContent(from = Bicicleta.class)}),
                    @OpenApiResponse(status = "404", content = {@OpenApiContent(from = RespostaErro.class)})
            }
    )

        public static void obterBicicleta (Context ctx) {  
            Bicicleta bicicleta = BicicletaService.acharBicicletaPorId(caminhoValidoParametroId(ctx));
            if (bicicleta == null) {
                throw new NotFoundResponse(NOT_FOUND_RESPONSE);
            } else {
                ctx.json(bicicleta);
            }

        }
    
        // Alterar status da bicicleta
        
        @OpenApi(
            summary = "Editar status da bicicleta",
            operationId = "alterarStatusBicicleta",
            path = "/Bicicleta/:id",
            method = HttpMethod.PATCH,
            pathParams = {@OpenApiParam(name = "id", type = Integer.class, description = "ID de bicicleta"), 
            @OpenApiParam(name = "status", type = String.class, description = "Status da bicicleta") },           
            tags = {"Bicicleta"},
            requestBody = @OpenApiRequestBody(content = {@OpenApiContent(from = RequisicaoNovaBicicleta.class)}),
            responses = {
                    @OpenApiResponse(status = "200"),
                    @OpenApiResponse(status = "404", content = {@OpenApiContent(from = RespostaErro.class)}),
                    @OpenApiResponse(status = "422", content = {@OpenApiContent(from = RespostaErro.class)})
            }
    )

        public static void alterarStatusBicicleta (Context ctx) {
            Bicicleta bicicleta = BicicletaService.acharBicicletaPorId(caminhoValidoParametroId(ctx));
            if (bicicleta == null) {
                throw new NotFoundResponse(NOT_FOUND_RESPONSE);
            } else {
                BicicletaService.atualizarStatusBicicleta(bicicleta.id, bicicleta.marca, bicicleta.modelo, bicicleta.ano, bicicleta.numero, ctx.queryParam("status"));
                ctx.status(200);
            }
    
        }

        // Recupera bicicletas cadastradas
    
        @OpenApi(
            summary = "Recuperar todas as bicicletas cadastradas",
            operationId = "recuperaBicicletas",
            path = "/Bicicleta",
            method = HttpMethod.GET,
            tags = {"Bicicleta"},
            responses = {
                    @OpenApiResponse(status = "200", content = {@OpenApiContent(from = Bicicleta[].class)})
            }
    )

        public static void recuperaBicicletas (Context ctx) {
            ctx.json(BicicletaService.retornaColecao());
            ctx.status(200);
        }

        // Checar e retornar caminho valido do parametro Id
        private static int caminhoValidoParametroId(Context ctx) {
            return ctx.pathParam("id", Integer.class).check(id -> id > 0).get();
        }
}
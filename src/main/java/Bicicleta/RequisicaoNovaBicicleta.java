package Bicicleta;

public class RequisicaoNovaBicicleta {
    
	public int id;
    public String marca;
    public String modelo;
    public String ano;
    public double numero;
	public String status;
	
	public void Bicicleta(int id, String marca, String modelo, String ano, int numero, String status) {
		this.id = id;
        this.marca = marca;
        this.modelo = modelo;
        this.ano = ano;
        this.numero = numero;
        this.status = status;
	}

}

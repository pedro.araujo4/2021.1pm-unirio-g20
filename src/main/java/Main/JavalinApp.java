package Main;

import io.javalin.Javalin;
import io.javalin.plugin.openapi.OpenApiOptions;
import io.javalin.plugin.openapi.OpenApiPlugin;
import io.javalin.plugin.openapi.ui.ReDocOptions;
import io.javalin.plugin.openapi.ui.SwaggerOptions;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.models.info.Info;

import static io.javalin.apibuilder.ApiBuilder.*;

import Bicicleta.BicicletaController;
import Totem.TotemController;
import Tranca.TrancaController;

public class JavalinApp {
    

   private Javalin app = 
   Javalin.create(config -> {
       config.registerPlugin(getConfiguredOpenApiPlugin());
       config.defaultContentType = "application/json";
   }).routes(() -> {
       path("Bicicleta", () -> {
           get(BicicletaController::recuperaBicicletas);
           post(BicicletaController::cadastrarBicicleta);
           path("/Bicicleta/:id", () -> {
               get(BicicletaController::editarBicicleta);
               delete(BicicletaController::removerBicicleta);
               get(BicicletaController::obterBicicleta);
               patch(BicicletaController::alterarStatusBicicleta);
       });          
            
       path("Totem", () -> {
           get(TotemController::recuperaTotensCadastrados);
           post(TotemController::incluirTotem);
           path("Totem/:id", () -> {
               patch(TotemController::editarTotem);  
               delete(TotemController::removerTotem);
               get(TotemController::listaTrancasDeUmTotem);
       });  
            
       path("Tranca", () -> {
        post(TrancaController::cadastrarTranca);
        path("Tranca/:id", () -> {
            patch(TrancaController::editarTranca);  
            delete(TrancaController::removerTranca);
            get(TrancaController::obterTranca);
            get(TrancaController::obterBicicletaTranca);
            patch(TrancaController::alterarStatusTranca);
    });  

});
}); 
});
});

public void start (int port) {
    this.app.start(port);
}

public void stop() {
    this.app.stop();
}

 static OpenApiPlugin getConfiguredOpenApiPlugin() {
     Info info = new Info().version("1.0").description("User API");
     OpenApiOptions options = new OpenApiOptions(info)
             .activateAnnotationScanningFor("Tranca", "Totem")
             .path("/swagger-docs") // endpoint for OpenAPI json
             .swagger(new SwaggerOptions("/swagger-ui")) // endpoint for swagger-ui
             .reDoc(new ReDocOptions("/redoc")) // endpoint for redoc
             .defaultDocumentation(doc -> {
                 doc.json("500", ApiResponse.class);
                 doc.json("503", ApiResponse.class);
             });
     return new OpenApiPlugin(options);
 }
}          



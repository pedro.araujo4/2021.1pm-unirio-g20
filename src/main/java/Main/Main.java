package Main;

public class Main {
    public static void main(String[] args) {
        int port = 7000; 
    	JavalinApp app = new JavalinApp();
    	app.start(getHerokuAssignedPort(port));
    }
    
    static int getHerokuAssignedPort(int port) {
        String herokuPort = System.getenv("7000");
        if (herokuPort != null) {
          return Integer.parseInt(herokuPort);
        }
        return port;
      }


    }
    

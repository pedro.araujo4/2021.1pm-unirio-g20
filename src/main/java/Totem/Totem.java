package Totem;
import Tranca.Tranca;
import java.util.List;


public class Totem {
    public int id;
	public String localizacao;
	public List<Tranca> trancas; 
	
	public Totem(int id, String localizacao) {
		this.id = id;
		this.localizacao = localizacao;
	}
}
package Totem;

import Main.RespostaErro;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import io.javalin.plugin.openapi.annotations.*;

public class TotemController {

    private TotemController() {
        throw new IllegalStateException("Classe de TotemController");
      }

    static final String NOT_FOUND_RESPONSE = "Totem não foi encontrado";

    // Incluir totem

    @OpenApi(
        summary = "Incluir Totem",
        operationId = "incluirTotem",
        path = "/Totem",
        method = HttpMethod.POST,
        tags = {"Totem"},
        requestBody = @OpenApiRequestBody(content = {@OpenApiContent(from = RequisicaoNovoTotem.class)}),
        responses = {
                @OpenApiResponse(status = "200"),
                @OpenApiResponse(status = "422", content = {@OpenApiContent(from = RespostaErro.class)})
        }
)
    public static void incluirTotem (Context ctx) {
        RequisicaoNovoTotem totem = ctx.bodyAsClass(RequisicaoNovoTotem.class);
        TotemService.adicionarTotem(totem.id, totem.localizacao);
        ctx.status(201);
    }
    
    // Editar totem

    @OpenApi(
        summary = "Editar Totem",
        operationId = "editarTotem",
        path = "/Totem/:id",
        method = HttpMethod.PATCH,
        pathParams = {@OpenApiParam(name = "id", type = Integer.class, description = "ID de totem")},
        tags = {"Totem"},
        requestBody = @OpenApiRequestBody(content = {@OpenApiContent(from = RequisicaoNovoTotem.class)}),
        responses = {
                @OpenApiResponse(status = "200"),
                @OpenApiResponse(status = "404", content = {@OpenApiContent(from = RespostaErro.class)}),
                @OpenApiResponse(status = "422", content = {@OpenApiContent(from = RespostaErro.class)})
        }
)

    public static void editarTotem (Context ctx) {
        Totem totem = TotemService.acharTotemPorID(caminhoValidoParametroId(ctx));
        if (totem == null) {
            throw new NotFoundResponse(NOT_FOUND_RESPONSE);
        } else {
            RequisicaoNovoTotem novoTotem = ctx.bodyAsClass(RequisicaoNovoTotem.class);
            TotemService.atualizarTotem(novoTotem.id, novoTotem.localizacao);
            ctx.status(200);
        }

    }

    // Remover totem

    @OpenApi(
        summary = "Remove totem por ID",
        operationId = "removerTotem",
        path = "/Totem/:id",
        method = HttpMethod.DELETE,
        pathParams = {@OpenApiParam(name = "id", type = Integer.class, description = "ID do totem")},
        tags = {"Totem"},
        responses = {
                @OpenApiResponse(status = "200"),
                @OpenApiResponse(status = "404", content = {@OpenApiContent(from = RespostaErro.class)})
        }
)

    public static void removerTotem (Context ctx) {  
        Totem totem = TotemService.acharTotemPorID(caminhoValidoParametroId(ctx));
        if (totem == null) {
            throw new NotFoundResponse(NOT_FOUND_RESPONSE);
        } else {
            TotemService.removerTotem(totem.id);
            ctx.status(200);
        }  

    }

    // Recuperar totens cadastrados

    @OpenApi(
        summary = "Recuperar todos os totens cadastrados",
        operationId = "recuperaTotensCadastrados",
        path = "/Totem",
        method = HttpMethod.GET,
        tags = {"Totem"},
        responses = {
                @OpenApiResponse(status = "200", content = {@OpenApiContent(from = Totem[].class)})
        }
)

    public static void recuperaTotensCadastrados (Context ctx) {
        ctx.json(TotemService.retornaColecao());
        ctx.status(200);
    } 

    // Listar trancas de um totem

    @OpenApi(
        summary = "Listar trancas de um totem",
        operationId = "listaTrancasDeUmTotem",
        path = "/Totem/:id/trancas",
        method = HttpMethod.GET,
        pathParams = {@OpenApiParam(name = "id", type = Integer.class, description = "id de um totem")},
        tags = {"Totem"},
        responses = {
                @OpenApiResponse(status = "200"), 
                @OpenApiResponse(status = "404", content = {@OpenApiContent(from = RespostaErro.class)}),
                @OpenApiResponse(status = "422", content = {@OpenApiContent(from = RespostaErro.class)})
        }
)
    public static void listaTrancasDeUmTotem (Context ctx) {
        Totem totem = TotemService.acharTotemPorID(caminhoValidoParametroId(ctx));
        if (totem==null){
            throw new NotFoundResponse(NOT_FOUND_RESPONSE);
        }
        else{
            TotemService.listaTrancasDeUmTotem(totem.id);
        }
    }

    // Checar e retornar caminho valido do parametro Id
    public static int caminhoValidoParametroId(Context ctx) {
    return ctx.pathParam("id", Integer.class).check(id -> id > 0).get();
}   


}    


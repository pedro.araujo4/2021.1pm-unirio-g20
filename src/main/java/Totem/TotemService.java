package Totem;

import Tranca.Tranca;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import io.javalin.http.NotFoundResponse;

public class TotemService {

    private TotemService() {
        throw new IllegalStateException("Classe de TotemService");
      }

    private static Map<Integer, Totem> redeDeTotens = new HashMap<>();
    private static AtomicInteger ultimoId;

    static {
        redeDeTotens.put(0, new Totem(0, "Barra"));
        redeDeTotens.put(1, new Totem(1, "Botafogo"));
        redeDeTotens.put(2, new Totem(2, "Tijuca"));
        ultimoId = new AtomicInteger(redeDeTotens.size());
    }

    // Adiciona um totem a rede de totens

    public static void adicionarTotem(int id, String localizacao) {
        int idAdc = ultimoId.incrementAndGet();
        redeDeTotens.put(idAdc, new Totem(id, localizacao));
    }

    // Retorna uma coleção de totens

    public static Collection<Totem> retornaColecao() {
        return redeDeTotens.values();
    }

    // Edita um totem

    public static void atualizarTotem(int id, String localizacao) {
        redeDeTotens.put(id, new Totem(id, localizacao));
    }

    // Acha um totem por ID

    public static Totem acharTotemPorID(int id) {
        return redeDeTotens.get(id);
    }

    // Retorna trancas em um determinado totem

    public static List<Tranca> listaTrancasDeUmTotem(int id) {
        Totem totem = acharTotemPorID(id);
        if (totem != null){
            return totem.trancas;
        }
        else{
            throw new NotFoundResponse("Id de totem inválido para procura");
        }
    }

    // Remove um totem da rede de totens

    public static void removerTotem(int id) {
        redeDeTotens.remove(id);
    }
    
    
}

package Tranca;

import Bicicleta.Bicicleta;
import io.javalin.http.NotFoundResponse;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class TrancaService {

    private TrancaService() {
        throw new IllegalStateException("Classe de TrancaService");
      }   

    private static Map<Integer, Tranca> redeDeTrancas = new HashMap<>();
    private static AtomicInteger ultimoId;

    static final String STATUS_ATIVA = "ATIVA";
    static final String STATUS_INATIVA = "INATIVA";

    static {
        redeDeTrancas.put(0, new Tranca(0, 1234, 1, "Barra", "1997", "Dobravel", STATUS_ATIVA));
        redeDeTrancas.put(1, new Tranca(1, 1235, 2, "Botafogo", "2007", "Manual", STATUS_INATIVA));
        redeDeTrancas.put(2, new Tranca(2, 1236, 3, "Tijuca", "2010", "Hibrida", STATUS_INATIVA));
        redeDeTrancas.put(3, new Tranca(3, 1237, 4, "Copacabana", "2014", "Aero", STATUS_ATIVA));
        redeDeTrancas.put(4, new Tranca(4, 1238, 5, "Centro", "2017", "Eletrica", STATUS_ATIVA));
        redeDeTrancas.put(5, new Tranca(5, 1239, 6, "Niterói", "2018", "Dobravel", STATUS_INATIVA));
        ultimoId = new AtomicInteger(redeDeTrancas.size());
    }

    // Adiciona uma tranca a rede de trancas

    public static void adicionarTranca(int id, int idBicicleta, int numero, String localizacao, String anoDeFabricacao, String modelo, String status) {
        int idAdc = ultimoId.incrementAndGet();
        redeDeTrancas.put(idAdc, new Tranca(id, idBicicleta, numero, localizacao, anoDeFabricacao, modelo, status));
    }

    // Retorna uma coleção de trancas

    public static Collection<Tranca> retornaColecao() {
        return redeDeTrancas.values();
    }

    // Edita uma tranca

    public static void atualizarTranca(int id, int idBicicleta, int numero, String localizacao, String anoDeFabricacao, String modelo, String status) {
        redeDeTrancas.put(id, new Tranca(id, idBicicleta, numero, localizacao, anoDeFabricacao, modelo, status));
    }

    // Edita um status de uma tranca

    public static void atualizarStatusTranca(int id, int idBicicleta, int numero, String localizacao, String anoDeFabricacao, String modelo, String status) {
        redeDeTrancas.put(id, new Tranca(id, idBicicleta, numero, localizacao, anoDeFabricacao, modelo, status));
        }

    // Acha uma tranca por id

    public static Tranca acharTrancaPorId(int id) {
        return redeDeTrancas.get(id);
    }

    //Adiciona uma bicicleta a tranca

    public static void adicionarBicicletaTranca (int id, Bicicleta bicicleta){
        Tranca tranca = acharTrancaPorId(id);
        if(tranca != null && tranca.status.equals(STATUS_INATIVA)){
             tranca.bicicleta=bicicleta; 
        }
    }

    // Obtem uma bicicleta na tranca

    public static Bicicleta obterBicicletaTranca (int id){
        Tranca tranca = acharTrancaPorId(id);
        if(tranca != null && tranca.status.equals(STATUS_INATIVA)){
             return tranca.obterBicicleta(id);
        }
        else{
           throw new NotFoundResponse("Bicicleta não encontrada"); 
        }
    }

    // Remove uma bicicleta em uma determinada tranca
    
    public static void removerBicicletaTranca (int id, int idBicicleta){
        Tranca tranca = acharTrancaPorId(id); 
        if(tranca != null && tranca.status.equals(STATUS_INATIVA)){
             tranca.removerBicicleta(idBicicleta); 
        }

    }


    // Remove uma tranca da rede de trancas

    public static void removerTranca(int id) {
        redeDeTrancas.remove(id);
    }
    

}


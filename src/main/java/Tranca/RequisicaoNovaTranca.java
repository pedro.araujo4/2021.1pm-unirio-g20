package Tranca;

public class RequisicaoNovaTranca {
    
    public int id;
	public int idBicicleta;
    public int numero;
    public String localizacao;
    public String anoDeFabricacao;
    public String modelo;
    public String status;
	
	public void Tranca(int id, int idBicicleta, int numero, String localizacao, String anoDeFabricacao, String modelo, String status) {
		this.id = id;
        this.idBicicleta = idBicicleta;
        this.numero = numero;
		this.localizacao = localizacao;
        this.anoDeFabricacao = anoDeFabricacao;
        this.modelo = modelo;
        this.status = status;
	}

}

package Tranca;

import Main.RespostaErro;
import Totem.TotemController;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import io.javalin.plugin.openapi.annotations.*;

public class TrancaController {
    
    private TrancaController() {
        throw new IllegalStateException("Classe de TrancaController");
      }
    
static final String NOT_FOUND_RESPONSE = "Tranca não foi encontrada";

    // Cadastra tranca

    @OpenApi(
        summary = "Cadastrar tranca",
        operationId = "cadastrarTranca",
        path = "/Tranca",
        method = HttpMethod.POST,
        tags = {"Tranca"},
        requestBody = @OpenApiRequestBody(content = {@OpenApiContent(from = RequisicaoNovaTranca.class)}),
        responses = {
                @OpenApiResponse(status = "200"),
                @OpenApiResponse(status = "422", content = {@OpenApiContent(from = RespostaErro.class)})
        }
)

    public static void cadastrarTranca (Context ctx) {
        RequisicaoNovaTranca tranca = ctx.bodyAsClass(RequisicaoNovaTranca.class);
        TrancaService.adicionarTranca(tranca.id, tranca.idBicicleta, tranca.numero, tranca.localizacao, tranca.anoDeFabricacao, tranca.modelo, tranca.status);
        ctx.status(200);
    }

    // Edita tranca
   
    @OpenApi(
        summary = "Editar Tranca",
        operationId = "editarTranca",
        path = "/Tranca/:id",
        method = HttpMethod.PATCH,
        pathParams = {@OpenApiParam(name = "id", type = Integer.class, description = "id de tranca")},
        tags = {"Tranca"},
        requestBody = @OpenApiRequestBody(content = {@OpenApiContent(from = RequisicaoNovaTranca.class)}),
        responses = {
                @OpenApiResponse(status = "200"),
                @OpenApiResponse(status = "404", content = {@OpenApiContent(from = RespostaErro.class)}),
                @OpenApiResponse(status = "422", content = {@OpenApiContent(from = RespostaErro.class)})
        }
)
    public static void editarTranca (Context ctx) {
        Tranca tranca = TrancaService.acharTrancaPorId(caminhoValidoParametroId(ctx));
        if (tranca == null) {
            throw new NotFoundResponse(NOT_FOUND_RESPONSE);
        } else {
            RequisicaoNovaTranca novaTranca = ctx.bodyAsClass(RequisicaoNovaTranca.class);
            TrancaService.atualizarTranca(novaTranca.id, novaTranca.idBicicleta, novaTranca.numero, novaTranca.localizacao, novaTranca.anoDeFabricacao, novaTranca.modelo, novaTranca.status);
            ctx.status(200);
        }    
    }

    // Remove tranca

    @OpenApi(
        summary = "Remove tranca por ID",
        operationId = "removerTranca",
        path = "/Tranca/:id",
        method = HttpMethod.DELETE,
        pathParams = {@OpenApiParam(name = "id", type = Integer.class, description = "id da Tranca")},
        tags = {"Tranca"},
        responses = {
                @OpenApiResponse(status = "200"),
                @OpenApiResponse(status = "404", content = {@OpenApiContent(from = RespostaErro.class)})
        }
)

    public static void removerTranca (Context ctx) {
        Tranca tranca = TrancaService.acharTrancaPorId(caminhoValidoParametroId(ctx));
        if (tranca == null) {
            throw new NotFoundResponse(NOT_FOUND_RESPONSE);
        } else {
            TrancaService.removerTranca(tranca.id);
            ctx.status(200);
        }  
    
    }

       
    // Obtem tranca

    @OpenApi(
        summary = "Obter tranca por ID",
        operationId = "obterTranca",
        path = "/Tranca/:id",
        method = HttpMethod.GET,
        pathParams = {@OpenApiParam(name = "id", type = Integer.class, description = "id da tranca")},
        tags = {"Tranca"},
        responses = {
                @OpenApiResponse(status = "200", content = {@OpenApiContent(from = Tranca.class)}),
                @OpenApiResponse(status = "404", content = {@OpenApiContent(from = RespostaErro.class)})
        }
)


    public static void obterTranca (Context ctx) {
        Tranca tranca = TrancaService.acharTrancaPorId(caminhoValidoParametroId(ctx));
        if (tranca == null) {
            throw new NotFoundResponse(NOT_FOUND_RESPONSE);
        } else {
            ctx.json(tranca);
        }


    }

    // Obtem bicicleta na tranca

    @OpenApi(
        summary = "Receber bicicleta da tranca",
        operationId = "obterBicicletaTranca",
        path = "/Tranca/:id/bicicleta",
        method = HttpMethod.PATCH,
        pathParams = {@OpenApiParam(name = "id", type = Integer.class, description = "id da tranca"), 
        },           
        tags = {"Tranca"},
        responses = {
                @OpenApiResponse(status = "200"),
                @OpenApiResponse(status = "404", content = {@OpenApiContent(from = RespostaErro.class)}),
                @OpenApiResponse(status = "422", content = {@OpenApiContent(from = RespostaErro.class)})
        }
    )
   public static void obterBicicletaTranca (Context ctx) {
    Tranca tranca = TrancaService.acharTrancaPorId(TotemController.caminhoValidoParametroId(ctx));
    if (tranca==null){
        throw new NotFoundResponse(NOT_FOUND_RESPONSE);  //fracasso
    }
    else{
        ctx.json(TrancaService.obterBicicletaTranca(TrancaController.caminhoValidoParametroId(ctx)));
        ctx.status(200); //sucesso
    }
   }

    // Altera status na tranca
 
    @OpenApi(
        summary = "Editar status da tranca",
        operationId = "alterarStatusTranca",
        path = "/Tranca/:id",
        method = HttpMethod.PATCH,
        pathParams = {@OpenApiParam(name = "id", type = Integer.class, description = "id da tranca"), 
        @OpenApiParam(name = "status", type = String.class, description = "Status da tranca") },           
        tags = {"Tranca"},
        requestBody = @OpenApiRequestBody(content = {@OpenApiContent(from = RequisicaoNovaTranca.class)}),
        responses = {
                @OpenApiResponse(status = "200"),
                @OpenApiResponse(status = "404", content = {@OpenApiContent(from = RespostaErro.class)}),
                @OpenApiResponse(status = "422", content = {@OpenApiContent(from = RespostaErro.class)})
        }
)

    public static void alterarStatusTranca (Context ctx) {
        Tranca tranca = TrancaService.acharTrancaPorId(caminhoValidoParametroId(ctx));
        if (tranca == null) {
            throw new NotFoundResponse(NOT_FOUND_RESPONSE);
        } else {
            TrancaService.atualizarStatusTranca(tranca.id, tranca.idBicicleta, tranca.numero, tranca.localizacao, tranca.anoDeFabricacao, tranca.modelo, ctx.queryParam("status"));
            ctx.status(200);
        }
    
    }

    
    // Checa e retorna caminho valido do parametro Id
    private static int caminhoValidoParametroId(Context ctx) {
        return ctx.pathParam("id", Integer.class).check(id -> id > 0).get();
    }   
}

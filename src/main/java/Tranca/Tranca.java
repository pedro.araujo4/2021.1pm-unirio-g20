package Tranca;
import Bicicleta.Bicicleta;
import io.javalin.http.NotFoundResponse;

public class Tranca {

    public int id;
	public int idBicicleta;
    public int numero;
    public String localizacao;
    public String anoDeFabricacao;
    public String modelo;
    public String status;
    public Bicicleta bicicleta;
	
	public Tranca(int id, int idBicicleta, int numero, String localizacao, String anoDeFabricacao, String modelo, String status) {
		this.id = id;
        this.idBicicleta = idBicicleta;
        this.numero = numero;
		this.localizacao = localizacao;
        this.anoDeFabricacao = anoDeFabricacao;
        this.modelo = modelo;
        this.status = status;
	}

    public Bicicleta obterBicicleta (int id){
        if (id == idBicicleta){
            return bicicleta;
        }
        else{
            throw new NotFoundResponse("Tranca não encontrada");
        }
    }

    public void removerBicicleta (int idBicicleta){
        if (bicicleta.id == idBicicleta){
            bicicleta = null;
        }
        else{
            throw new NotFoundResponse("Remoção de bicicleta não sucedida. Revise a bicicleta");
        }
    }

}
    
    
